.. -*- mode: rst -*-

cppyy: Python-C++ bindings interface based on Cling/LLVM
========================================================

cppyy provides dynamic Python-C++ bindings by leveraging the Cling C++
interpreter and LLVM.
It supports both PyPy and CPython.

Details and performance are described in
`this paper <http://cern.ch/wlav/Cppyy_LavrijsenDutta_PyHPC16.pdf>`_.

Full documentation: `cppyy.readthedocs.org <http://cppyy.readthedocs.io/en/latest/>`_.

